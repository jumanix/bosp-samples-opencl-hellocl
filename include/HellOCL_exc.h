/**
 *       @file  HellOCL_exc.h
 *      @brief  The HellOCL BarbequeRTRM application
 *
 * Description: to be done...
 *
 *     @author  Name Surname (nickname), your@email.com
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 20XX, Name Surname
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef HELLOCL_EXC_H_
#define HELLOCL_EXC_H_

#include <bbque/bbque_exc.h>
#ifndef __OPENCL_CL_H 
#include <CL/cl.h>
#endif //__OPENCL_CL_H 

using bbque::rtlib::BbqueEXC;

class HellOCL : public BbqueEXC {

public:

	HellOCL(std::string const & name,
			std::string const & recipe,
			RTLIB_Services_t *rtlib);

	~HellOCL();


private:

	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(int8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();
	RTLIB_ExitCode_t onRelease();

	void oclCleanup();

	int err;
	unsigned int n;

	size_t in_sz;
	size_t out_sz;

	char * in;
	char * out;

	cl_context ctx;
	cl_command_queue cmdq;

	cl_mem in_buf;
	cl_mem out_buf;

	size_t src_sz;
	cl_program prg;
	cl_kernel krn;

	cl_event ev[10];
};

#endif // HELLOCL_EXC_H_

