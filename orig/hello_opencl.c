/* hello_opencl.c */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <CL/cl.h>

int main()
{
	int err;
	unsigned int n;

	cl_uint nplatforms;
	cl_platform_id* platforms;
	cl_platform_id platform;

	clGetPlatformIDs( 0,0,&nplatforms);
	platforms = (cl_platform_id*)malloc(nplatforms*sizeof(cl_platform_id));
	clGetPlatformIDs( nplatforms, platforms, 0);
/*
	for(i=0; i<nplatforms; i++) {
		platform = platforms[i];
		clGetPlatformInfo(platforms[i],CL_PLATFORM_NAME,256,buffer,0);
		if (!strcmp(buffer,"coprthr-e")) break;
	}
*/
	//if (i<nplatforms)
	platform = platforms[0];
	fprintf(stderr, "Device selected\n");
	//else exit(1);

	cl_uint ndevices;
	cl_device_id* devices;
	cl_device_id dev;

	clGetDeviceIDs(platform,CL_DEVICE_TYPE_ALL,0,0,&ndevices);
	devices = (cl_device_id*)malloc(ndevices*sizeof(cl_device_id));
	clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL,ndevices,devices,0);
	fprintf(stderr, "Device selected\n");

	if (ndevices) dev = devices[0];
	else exit(1);

	cl_context_properties ctxprop[3] = {
		(cl_context_properties)CL_CONTEXT_PLATFORM,
		(cl_context_properties)platform,
		(cl_context_properties)0
	};
	cl_context ctx = clCreateContext(ctxprop,1,&dev,0,0,&err);
	cl_command_queue cmdq = clCreateCommandQueue(ctx,dev,0,&err);

	const char input[] = "HELLo_RTRM";
	n = strlen(input);

	size_t in_sz  = n*sizeof(char);
	size_t out_sz = n*sizeof(char);
	char * in  = (char *) malloc(in_sz);
	char * out = (char *) malloc(out_sz);

	strncpy(in, input, n);
	strncpy(out, " ", n);

	cl_mem in_buf  = clCreateBuffer(ctx,CL_MEM_USE_HOST_PTR, in_sz, in, &err);
	cl_mem out_buf = clCreateBuffer(ctx,CL_MEM_USE_HOST_PTR, out_sz, out, &err);

	const char kernel_code[] =
		 "__kernel void hello_bbq_world(\n"
		 "   uint n,__global char* in, __global char* c )\n"
		 "{\n"
		 "     int i = get_global_id(0);\n"
		 "     c[i]  = in[n-1-i];\n"
		 "}\n";

	const char* src[1] = { kernel_code };
	size_t src_sz = sizeof(kernel_code);

	cl_program prg = clCreateProgramWithSource(ctx,1,(const char**)&src,&src_sz,&err);
	clBuildProgram(prg,1,&dev,0,0,0);
	cl_kernel krn = clCreateKernel(prg,"hello_bbq_world",&err);
	if (err != NULL)
		 fprintf(stderr, "Error in kernel creation\n");

	clSetKernelArg(krn,0,sizeof(cl_uint),&n);
	clSetKernelArg(krn,1,sizeof(cl_mem), &in_buf);
	clSetKernelArg(krn,2,sizeof(cl_mem), &out_buf);

	size_t gtdsz[] = { n };
	size_t ltdsz[] = { n };
	cl_event ev[10];

	clEnqueueNDRangeKernel(cmdq,krn,1,0,gtdsz,ltdsz,0,0,&ev[0]);
	clEnqueueReadBuffer(cmdq,out_buf,CL_TRUE,0,out_sz, out,0,0,&ev[1]);

	err = clWaitForEvents(2,ev);
	printf("clWaitForEvents returned %d\n", err);
	int i;
	//for(i=0;i<n;i++) printf("out[%d] %c\n",i, out[i]);
	out[n]='\0';
	fprintf(stderr, "Someone says '%s'\n", out);

	clReleaseEvent(ev[1]);
	clReleaseEvent(ev[0]);
	clReleaseKernel(krn);
	clReleaseProgram(prg);
	clReleaseMemObject(in_buf);
	clReleaseMemObject(out_buf);
	clReleaseCommandQueue(cmdq);
	clReleaseContext(ctx);

	free(in);
	free(out);

}

