# README #

This application is a demo sample, part of the [BarbequeRTRM framework](https://bitbucket.org/bosp). To build and run it follow the steps below:

* Rename the directory into **HellOCL**
* Move the directory under bosp/contrib/user/
* From bosp/ run 'make bootstrap'
* 'make menuconfig' --> Applications --> [*] HellOCL to enable the building
* make
* source out/etc/bbque/bosp_init.env
* hellocl