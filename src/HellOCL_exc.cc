/**
 *       @file  HellOCL_exc.cc
 *      @brief  The HellOCL BarbequeRTRM application
 *
 * Description: to be done...
 *
 *     @author  Name Surname (nickname), your@email.com
 *
 *     Company  Your Company
 *   Copyright  Copyright (c) 20XX, Name Surname
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */


#include "HellOCL_exc.h"

#include <cstdio>
#include <cstring>
#include <bbque/utils/utility.h>

static const char input[] = "HELLo_RTRM";

static const char kernel_code[] =
		 "__kernel void hello_bbq_world(\n"
		 "   uint n,__global char* in, __global char* c )\n"
		 "{\n"
		 "     int i = get_global_id(0);\n"
		 "     c[i]  = in[n-1-i];\n"
		 "}\n";


HellOCL::HellOCL(std::string const & name,
		std::string const & recipe,
		RTLIB_Services_t *rtlib) :
	BbqueEXC(name, recipe, rtlib, RTLIB_LANG_OPENCL) {

	logger->Warn("New HellOCL::HellOCL()");

	// NOTE: since RTLib 1.1 the derived class construct should be used
	// mainly to specify instantiation parameters. All resource
	// acquisition, especially threads creation, should be palced into the
	// new onSetup() method.

	logger->Info("EXC Unique IDentifier (UID): %u", GetUid());

}

HellOCL::~HellOCL() {
	if (logger != nullptr)
		logger->Warn("Goodbye!");
	else
		fprintf(stderr, "Goodbye!\n");
}

RTLIB_ExitCode_t HellOCL::onSetup() {
	// This is just an empty method in the current implementation of this
	// testing application. However, this is intended to collect all the
	// application specific initialization code, especially the code which
	// acquire system resources (e.g. thread creation)
	logger->Warn("HellOCL::onSetup()");
	n = strlen(input);

	return RTLIB_OK;
}

RTLIB_ExitCode_t HellOCL::onConfigure(int8_t awm_id) {

	logger->Warn("HellOCL::onConfigure(): EXC [%s] => AWM [%02d]",
		exc_name.c_str(), awm_id);

	cl_uint nplatforms;
	cl_platform_id* platforms;
	cl_platform_id platform;

	// Platform
	clGetPlatformIDs( 0,0,&nplatforms);
	platforms = (cl_platform_id*)malloc(nplatforms*sizeof(cl_platform_id));
	clGetPlatformIDs( nplatforms, platforms, 0);
	logger->Info("OpenCL platforms: %d", nplatforms);
	if (nplatforms == 0)
		logger->Error("No platform!");

	platform = platforms[0];
	char pname[128];
	clGetPlatformInfo(platform, CL_PLATFORM_NAME, 128, pname, NULL);
	logger->Info("Platform assigned: %s", pname);

	cl_uint ndevices;
	cl_device_id* devices;
	cl_device_id dev;

	// Device
	clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL,0,0,&ndevices);
	devices = (cl_device_id*)malloc(ndevices*sizeof(cl_device_id));
	clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL,ndevices,devices,0);
	if (ndevices) {
		dev = devices[0];
		logger->Info("Device assigned");
	}
	else exit(1);

	if (Cycles() > 1) {
		oclCleanup();
		logger->Info("Cleanup performed");
	}

	// Context and command queue
	cl_context_properties ctxprop[3] = {
		(cl_context_properties)CL_CONTEXT_PLATFORM,
		(cl_context_properties)platform,
		(cl_context_properties)0
	};

	ctx  = clCreateContext(ctxprop,1,&dev,0,0,&err);
	if (err != 0)
		 logger->Error("Context creation failed: Error %d", err);


	cmdq = clCreateCommandQueue(ctx,dev,0,&err);
	logger->Info("Context and command queue created");

	in_sz  = n*sizeof(char);;
	out_sz = n*sizeof(char);
	in  = (char *) malloc(in_sz);
	out = (char *) malloc(out_sz);

	strncpy(in, input, n);
	strncpy(out, " ", n);

	// Memory buffers
	in_buf  = clCreateBuffer(ctx,CL_MEM_USE_HOST_PTR, in_sz,  in,  &err);
	out_buf = clCreateBuffer(ctx,CL_MEM_USE_HOST_PTR, out_sz, out, &err);
	logger->Info("Memory buffers ready");

	// Program
	const char* src[1] = { kernel_code };
	size_t src_sz = sizeof(kernel_code);
	prg = clCreateProgramWithSource(ctx,1,(const char**)&src,&src_sz,&err);
	logger->Info("Kernel code size: %d", src_sz);
	if (err != 0)
		 logger->Error("Program creation failed: Error %d", err);
	clBuildProgram(prg,1,&dev,0,0,0);

	// Kernel
	krn = clCreateKernel(prg,"hello_bbq_world",&err);
	if (err != 0)
		 logger->Error("Kernel creation failed: Error %d", err);

	// Kernel arguments
	clSetKernelArg(krn,0,sizeof(cl_uint),&n);
	clSetKernelArg(krn,1,sizeof(cl_mem), &in_buf);
	clSetKernelArg(krn,2,sizeof(cl_mem), &out_buf);

	return RTLIB_OK;
}

RTLIB_ExitCode_t HellOCL::onRun() {
	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();

	// Return after 5 cycles
	if (Cycles() >= 5)
		return RTLIB_EXC_WORKLOAD_NONE;

	size_t gtdsz[] = { n };
	size_t ltdsz[] = { n };

	// Do one more cycle
	logger->Warn("HellOCL::onRun()      : EXC [%s]  @ AWM [%02d]",
		exc_name.c_str(), wmp.awm_id);

//clEnqueueNDRangeKernel(cmdq,krn,1,0,gtdsz,ltdsz,0,0,&ev[0]);
	clEnqueueNDRangeKernel(cmdq,krn,1,0,gtdsz,NULL,0,0,&ev[0]);
	clEnqueueReadBuffer(cmdq,out_buf,CL_TRUE,0,out_sz, out,0,0,&ev[1]);
	//err = clWaitForEvents(2,ev);
	sleep(1);
	err =clFinish(cmdq);

	logger->Warn("clWaitForEvents returned %d", err);
	out[n]='\0';
	logger->Notice("Someone says '%s'\n", out);
	return RTLIB_OK;
}

RTLIB_ExitCode_t HellOCL::onMonitor() {
	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();

	logger->Warn("HellOCL::onMonitor()  : EXC [%s]  @ AWM [%02d], Cycle [%4d]",
		exc_name.c_str(), wmp.awm_id, Cycles());

	return RTLIB_OK;
}

RTLIB_ExitCode_t HellOCL::onRelease() {

	logger->Warn("HellOCL::onRelease()  : EXC [%s]",
		exc_name.c_str());
	oclCleanup();

	return RTLIB_OK;
}

void HellOCL::oclCleanup() {
	clReleaseEvent(ev[1]);
	clReleaseEvent(ev[0]);
	clReleaseKernel(krn);
	clReleaseProgram(prg);
	clReleaseMemObject(in_buf);
	clReleaseMemObject(out_buf);
	clReleaseCommandQueue(cmdq);
	clReleaseContext(ctx);
	free(in);
	free(out);
}
