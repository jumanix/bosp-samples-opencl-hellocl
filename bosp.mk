
ifdef CONFIG_SAMPLES_HELLOCL

# Targets provided by this project
.PHONY: samples_opencl_hellocl clean_samples_opencl_hellocl

# Add this to the "contrib_testing" target
samples_opencl: samples_opencl_hellocl
clean_samples_opencl: clean_samples_opencl_hellocl

MODULE_SAMPLES_USER_HELLOCL=samples/opencl/hellocl

samples_opencl_hellocl:
	@echo
	@echo "==== Building HellOCL ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(BOSP_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_SAMPLES_USER_HELLOCL)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_SAMPLES_USER_HELLOCL)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_SAMPLES_USER_HELLOCL)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS=$(TARGET_FLAGS) \
		CXX=$(CXX) CXXFLAGS=$(TARGET_FLAGS) \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_SAMPLES_USER_HELLOCL)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_samples_opencl_hellocl:
	@echo
	@echo "==== Clean-up HellOCL Application ===="
	@rm -rf $(MODULE_SAMPLES_USER_HELLOCL)/build
	@echo

else # CONFIG_SAMPLES_HELLOCL

samples_opencl_hellocl:
	$(warning HellOCL disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_SAMPLES_HELLOCL

